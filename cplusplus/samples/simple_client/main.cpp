
#include <iostream>

#include <vwnet/system.hpp>
#include <vwnet/socket.hpp>

int main(int argc, char ** argv) {
	vwnet::System::init();

	vwnet::ClientSocket * s = new vwnet::ClientSocket(vwnet::Socket::kTypeTcp);

	if (s->connectTo(vwnet::Url("localhost", 3550)) == vwnet::Socket::kNoError) {
		std::string buffer = "This is a test";

		int bytes = s->send(reinterpret_cast<const unsigned char*>(buffer.c_str()), buffer.length());
		std::cout << "Bytes sent: " << bytes << std::endl;
		
		static const int bufferSize = 512;
		unsigned char rcvBuffer[bufferSize];
		bytes = s->receive(rcvBuffer, bufferSize);
		if (bytes > 0) {
			rcvBuffer[bytes] = '\0';
			std::cout << "Echo received from server: " << rcvBuffer << std::endl;
		}
		else {
			std::cerr << "Error receiving response from server" << std::endl;
		}
		
		s->close();
	}

	delete s;

	vwnet::System::cleanup();
    return 0;
}
