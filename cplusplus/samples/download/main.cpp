//
//  main.cpp
//  vwnet
//
//  Created by Fernando Serrano Carpena on 28/9/15.
//  Copyright © 2015 Fernando Serrano Carpena. All rights reserved.
//

#include <iostream>
#include <iomanip>

#include <vwnet/download.hpp>

int main(int argc, const char * argv[]) {
    if (argc<3) {
        std::cerr << "Usage: download [remote-url] [local-destination-file]" << std::endl;
        exit(1);
    }
    
    {
		std::string src = argv[1];
		std::string dst = argv[2];
		vwnet::Session session;
        vwnet::Download dl(&session);
		session.start();

		dl.onComplete([](vwnet::Download * dl) {
			if (dl->isCanceled()) {
				std::cout << "Download cancelled" << std::endl;
			}
			else if (dl->isFail()) {
				std::cout << "Download failed" << std::endl;
			}
			else {
				std::cout << "Download completed" << std::endl;
			}
			
		});
		
		dl.onProgress([](vwnet::Download* dl) {
			std::string downloaded;
			std::string total;
			vwnet::Download::formatSize(dl->downloaded(), downloaded);
			vwnet::Download::formatSize(dl->total(), total);
			std::cout << "Downloading " << downloaded << " of " << total << ", "
					<< std::fixed << std::setprecision(2) << dl->completed() << "% completed" << std::endl;
		});
		
		std::cout << "Starting download" << std::endl;
        dl.start(src,dst);

		// The Download destructor will automatically join the download thread, so you only need to call waitUntilCompleted()
		// if you need to join the thread explicitly
		//
		//	dl.waitUntilCompleted();
		//
		
    }
	
	std::cout << "Testing synchronous download: " << std::endl;
	
	vwnet::Download syncDownload;
	syncDownload.onProgress([&](vwnet::Download * dl) {
		std::cout << "Downloading " << dl->completed() << "%" << std::endl;
	});
	syncDownload.downloadSync("http://apache.vitaminew.com/~bg2/test/img_1.jpg", "sync-test.jpg");
	
	std::cout << "Done" << std::endl;
	
    return 0;
}
