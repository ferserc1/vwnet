
#include <iostream>

#include <vwnet/socket.hpp>

int main(int argc, char ** argv) {
	vwnet::System::init();

	vwnet::ServerSocket * serverSocket = new vwnet::ServerSocket(vwnet::Socket::kTypeTcp);
	vwnet::Socket * clientSocket = nullptr;
	if (serverSocket->create(3550) == vwnet::Socket::kNoError &&
		(clientSocket = serverSocket->accept())) {
		const int bufflen = 512;
		unsigned char buffer[bufflen];
		int bytes = 0;

		do {
			bytes = clientSocket->receive(buffer, bufflen);
			if (bytes > 0) {
				buffer[bytes] = '\0';
				std::cout << "Message received: " << buffer << std::endl;
				bytes = clientSocket->send(buffer, bytes);
				if (bytes) {
					std::cout << "Bytes sent: " << bytes << std::endl;
				}
			}

			if (bytes == 0) {
				std::cout << "Connection closed" << std::endl;
			}
			else if (bytes<0) {
				std::cerr << "Error receiving data from client" << std::endl;
			}
		} while (bytes > 0);
		
		clientSocket->close();
		delete clientSocket;
	}

	serverSocket->close();
	delete serverSocket;

	vwnet::System::cleanup();

    return 0;
}
