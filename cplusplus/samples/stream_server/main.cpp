
#include <iostream>

#include <vwnet/socket.hpp>
#include <vwnet/stream_socket.hpp>

int main(int argc, char ** argv) {
	vwnet::System::init();

	vwnet::ServerSocket * serverSocket = new vwnet::ServerSocket(vwnet::Socket::kTypeTcp);
	vwnet::Socket * clientSocket = nullptr;
	if (serverSocket->create(3550) == vwnet::Socket::kNoError &&
		(clientSocket = serverSocket->accept())) {
		
		vwnet::StreamSocket ssock(clientSocket);

		try {
			char character;
			short littleNumber;
			int integer;
			float floatingPoint;
			double doublePrec;
			std::string message;

			ssock >> character >> littleNumber >> integer >> floatingPoint >> doublePrec >> message;

			std::cout << "Values received from client: "
				<< character << ", "
				<< littleNumber << ", "
				<< integer << ", "
				<< floatingPoint << ", "
				<< doublePrec << ", "
				<< message
				<< std::endl;


			character += 1;
			littleNumber += 1;
			integer += 1;
			floatingPoint += 1;
			doublePrec += 1;
			message = "greetings from server";

			ssock << character << littleNumber << integer << floatingPoint << doublePrec << message;

			std::cout << "Values sent to client: "
				<< character << ", "
				<< littleNumber << ", "
				<< integer << ", "
				<< floatingPoint << ", "
				<< doublePrec << ", "
				<< message
				<< std::endl;

		}
		catch (vwnet::error::net::connection & e) {
			std::cerr << "Connection error: " << e.what() << std::endl;
		}
		catch (vwnet::error::net::io & e) {
			std::cerr << e.what() << std::endl;
		}
				
		clientSocket->close();
		delete clientSocket;
	}

	serverSocket->close();
	delete serverSocket;

	vwnet::System::cleanup();

    return 0;
}
