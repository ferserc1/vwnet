//
//  main.cpp
//  vwnet
//
//  Created by Fernando Serrano Carpena on 28/9/15.
//  Copyright © 2015 Fernando Serrano Carpena. All rights reserved.
//

#include <iostream>

#include <vwnet/url.hpp>
#include <vwnet/http/request.hpp>

int main(int argc, const char * argv[]) {
	vwnet::Session session;
	session.start();

	vwnet::http::Request req(vwnet::http::Request::kMethodPost, &session);
	
	// Usage:
	//	Send a raw body (not available for kMethodGet)
	//
	//		req.setBody("{ \"param1\":\"value 1\", \"param2\":\"value 2\" }","application/json);
	//
	//	Parameters: if you use parameters, the body will be ignored. Parameters are sent in theURL
	//	if the request method is kMethodGet, and are encoded in the body for the rest of the methods
	//	using x-www-form-urlencoded
	//
	//		req.setParameter("param1", "value 1");
	//		req.setParameter("param2", "value 2");
	//
	//	You can also send GET parameters using the vwnet::Url class, but do not use both of them in the
	//	same request
	//
	//	You can set custom headers using:
	//
	//		req.setHeader("Header-Name","header value");
	//
	

	std::stringstream body;

	vwnet::Url url1("localhost");
	
	// The body is readed into a stream that could be, for example, a file
	req.load(url1, body);
	
	std::cout << "Status: " << req.getStatusCode() << std::endl;
	req.eachHeader([&](const std::string & header, const std::string & value) {
		std::cout << header << " => " << value << std::endl;
	});
	std::cout << body.str() << std::endl;

	// Test: login and request using sessions
	body.clear();
	vwnet::http::Request loginRequest(vwnet::http::Request::kMethodPost, &session);
	loginRequest.setParameter("username", "admin");
	loginRequest.setParameter("password", "1234");
	loginRequest.load("localhost:4000/rest/auth/local", body);
	if (loginRequest.getStatusCode()!=200) {
		std::cout << body.str() << std::endl;
	}
	
	body.clear();
	vwnet::http::Request dniRequest(vwnet::http::Request::kMethodGet, &session);
	dniRequest.load("localhost:4000/rest/user/12345", body);	// This is supposed to be a request that requires authentication
	std::cout << body.str() << std::endl;
	
    return 0;
}
