
#include <iostream>

#include <vwnet/system.hpp>
#include <vwnet/socket.hpp>
#include <vwnet/stream_socket.hpp>

int main(int argc, char ** argv) {
	vwnet::System::init();

	vwnet::ClientSocket * s = new vwnet::ClientSocket(vwnet::Socket::kTypeTcp);

	if (s->connectTo(vwnet::Url("localhost", 3550)) == vwnet::Socket::kNoError) {
		try {
			vwnet::StreamSocket ssock(s);

			char character = 'A';
			short littleNumber = 12500;
			int integer = 2456930;
			float floatingPoint = 13.55f;
			double doublePrec = 13.55;
			std::string helloString = "Hello from client";

			ssock << character << littleNumber << integer << floatingPoint << doublePrec << helloString;

			std::cout << "Values sent to server: "
				<< character << ", "
				<< littleNumber << ", "
				<< integer << ", "
				<< floatingPoint << ", "
				<< doublePrec << ", "
				<< helloString
				<< std::endl;

			ssock >> character;
			ssock >> littleNumber;
			ssock >> integer;
			ssock >> floatingPoint;
			ssock >> doublePrec;
			ssock >> helloString;

			std::cout << "Response from server: "
				<< character << ", "
				<< littleNumber << ", "
				<< integer << ", "
				<< floatingPoint << ", "
				<< doublePrec << ", "
				<< helloString
				<< std::endl;
		}
		catch (vwnet::error::net::connection & e) {
			std::cerr << e.what() << std::endl;
		}
		catch (vwnet::error::net::io & e) {
			std::cerr << e.what() << std::endl;
		}
		
		s->close();
	}

	delete s;

	vwnet::System::cleanup();
    return 0;
}
