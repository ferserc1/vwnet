
add_executable (sample_download_queue main.cpp)

target_link_libraries (sample_download_queue vwnet)

set_target_properties(sample_download_queue PROPERTIES DEBUG_POSTFIX "d")

install (TARGETS sample_download_queue DESTINATION bin)