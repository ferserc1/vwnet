//
//  main.cpp
//  vwnet
//
//  Created by Fernando Serrano Carpena on 28/9/15.
//  Copyright © 2015 Fernando Serrano Carpena. All rights reserved.
//

#include <iostream>
#include <iomanip>

#include <vwnet/download_queue.hpp>

int main(int argc, const char * argv[]) {
	
    {
		vwnet::DownloadQueue queue;
		queue.setMaxConcurrency(4);
		
		std::string url = "http://apache.vitaminew.com/~bg2/test/";
		queue.addResource(url + "img_1.jpg", "img_1.jpg");
		queue.addResource(url + "img_2.jpg", "img_2.jpg");
		queue.addResource(url + "img_3.jpg", "img_3.jpg");
		queue.addResource(url + "img_4.jpg", "img_4.jpg");
		queue.addResource(url + "img_5.jpg", "img_5.jpg");
		queue.addResource(url + "img_6.jpg", "img_6.jpg");
		queue.addResource(url + "img_7.jpg", "img_7.jpg");
		queue.addResource(url + "img_8.jpg", "img_8.jpg");
		queue.addResource(url + "img_9.jpg", "img_9.jpg");
		queue.addResource(url + "img_10.jpg", "img_10.jpg");
		
		queue.onComplete([](vwnet::DownloadQueue * dl) {
			if (dl->isCancelled()) {
				std::cout << "Download aborted" << std::endl;
			}
			else {
				std::cout << "Download completed" << std::endl;
			}
		});
		
		queue.onProgress([](vwnet::DownloadQueue * dl) {
			std::cout << "Downloading files: " << dl->completed() << " of " << dl->count() << std::endl;
			dl->eachDownload([](vwnet::Download * dl) {
				std::cout << "    " << dl->getSource() << ": "
						  << std::fixed << std::setprecision(2)
						  << dl->completed() << "%" << std::endl;
			});
		});
		
		// This function will lauch the download thread
		queue.start();
		
		// Optional: the DownloadQueue destructor will automatically join the download thread. Use waitUntilCompleted() only if you
		// explicitly want to join the download thread before the DownloadQueue is destroyed.
		//
		//		queue.waitUntilCompleted();
		//
	}

    return 0;
}
