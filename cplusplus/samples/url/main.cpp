//
//  main.cpp
//  vwnet
//
//  Created by Fernando Serrano Carpena on 28/9/15.
//  Copyright © 2015 Fernando Serrano Carpena. All rights reserved.
//

#include <iostream>

#include <vwnet/url.hpp>

int main(int argc, const char * argv[]) {
	vwnet::Url url1("google.com");
	vwnet::Url url2("myserver.mydomain.com", "path/to/resource");
	vwnet::Url url3("myserver.mydomain.com", 8080, "path/to/resource");
	vwnet::Url url4("https", "myserver.mydomain.com", 8080);
	vwnet::Url url5("https", "myserver.mydomain.com", 8080, "/resource");

	std::cout << url1.getString() << std::endl;
	std::cout << url2.getString() << std::endl;
	std::cout << url3.getString() << std::endl;
	std::cout << url4.getString() << std::endl;
	std::cout << url5.getString() << std::endl;

	url5.appendPath("another/");
	url5.appendPath("/resource");
	
	std::cout << url5.getString() << std::endl;

	url5.setUrlParameter("param1", "value1");
	url5.setUrlParameter("param2", "value2");

	std::cout << url5.getString() << std::endl;

	url5.setHashParameter("hash1", "value3");
	url5.setHashParameter("hash2", "value4");

	std::cout << url5.getString() << std::endl;

	url4.setHashParameter("hash1", "value1");
	
	std::cout << url4.getString() << std::endl;
	
	vwnet::Url url6 = url4;
	std::cout << url6.getString() << std::endl;

    return 0;
}
