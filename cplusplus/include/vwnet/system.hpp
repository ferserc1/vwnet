#ifndef VWNET_SYSTEM_HPP
#define VWNET_SYSTEM_HPP

#include <vwnet/export.hpp>

#if PLATFORM_WINDOWS==1

#define VWNET_WINDOWS 1
#define VWNET_MAC 0
#define VWNET_IOS 0
#define VWNET_IOS_SIMULATOR 0
#define VWNET_LINUX 0

typedef unsigned short uint16_t;
typedef unsigned int uint32_t;


#elif __APPLE__
#include "TargetConditionals.h"
#if TARGET_IPHONE_SIMULATOR

#define VWNET_WINDOWS 0
#define VWNET_MAC 1
#define VWNET_LINUX 0
#define VWNET_IOS 1
#define VWNET_IOS_SIMULATOR 1

#elif TARGET_OS_IPHONE

#define VWNET_WINDOWS 0
#define VWNET_MAC 1
#define VWNET_IOS 1
#define VWNET_IOS_SIMULATOR 0
#define VWNET_LINUX 0

#elif TARGET_OS_MAC

#define VWNET_WINDOWS 0
#define VWNET_MAC 1
#define VWNET_IOS 0
#define VWNET_IOS_SIMULATOR 0
#define VWNET_LINUX 0

#else

#define VWNET_WINDOWS 0
#define VWNET_MAC 0
#define VWNET_IOS 0
#define VWNET_IOS_SIMULATOR 0
#define VWNET_LINUX 0

#endif

#elif PLATFORM_LINUX==1

#define VWNET_WINDOWS 0
#define VWNET_MAC 0
#define VWNET_IOS 0
#define VWNET_IOS_SIMULATOR 0
#define VWNET_LINUX 1

#endif

namespace vwnet {

class VWNETEXPORT System {
public:
	static bool init();
	static void cleanup();
};

}

#endif