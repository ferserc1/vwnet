#ifndef _vwnet_export_hpp_
#define _vwnet_export_hpp_

#include <vwnet/platform.hpp>

namespace vwnet {

typedef void * impl_ptr;
template <class T> T impl_cast(void * nativePtr) { return static_cast<T>(nativePtr); }

}


#ifndef WIN32
#define VWNETEXPORT

#else

#pragma warning( disable: 4251 )
#pragma warning( disable: 4275 )

#if defined( VWNET_STATIC_LIB )
#define VWNETEXPORT

#elif defined( VWNET_DYNAMIC_LIB )
#define VWNETEXPORT __declspec(dllexport)

#else
#define VWNETEXPORT __declspec(dllimport)

#ifdef _MSC_VER
#ifdef _DEBUG
#pragma comment(lib ,"vwnetd")

#else
#pragma comment(lib, "vwnet")

#endif

#endif

#endif

#endif

#endif	// _vwnet_export_hpp_


