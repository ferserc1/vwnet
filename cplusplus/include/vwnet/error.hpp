#ifndef _VWNET_ERROR_HPP_
#define _VWNET_ERROR_HPP_

#include <vwnet/system.hpp>

#include <exception>

#if VWNET_WINDOWS==1
#ifndef noexcept
#define noexcept
#endif
#endif

namespace vwnet {

namespace error {

class base_exception : public std::exception {
public:
	explicit base_exception() :exception(), _what() {}
	explicit base_exception(const std::string & msg) :exception(), _what(msg) {}
	virtual ~base_exception() {}
	
	virtual const char * what() const noexcept { return _what.c_str(); }
	
protected:
	std::string _what;
};
	
namespace net {
	
	class connection : public base_exception {
	public:
		connection(const std::string & msg) :base_exception(msg) {}
	};

	class connection_closed : public connection {
	public:
		connection_closed() :connection("The connection was closed in the remote host") {}
	};

	class io : public base_exception {
	public:
		io(const std::string & msg) :base_exception(msg) { }
	};
	
	class read : public io {
	public:
		read() :io("Could not read from socket") {}
		read(const std::string &msg) :io(msg) {}
	};
	
	class write : public io {
	public:
		write() :io("Could not write to socket") {}
		write(const std::string & msg) :io(msg) {}
	};
}

template <class ExceptionClass>
class Assert {
public:
	Assert(bool condition) {
		if (!condition) throw ExceptionClass();
	}
	Assert(bool condition, const std::string & msg) {
		if (!condition) throw ExceptionClass(msg);
	}
};

template <class ExceptionClass>
class ThrowWhen {
public:
	ThrowWhen(bool condition) {
		if (condition) throw ExceptionClass();
	}
	ThrowWhen(bool condition, const std::string & msg) {
		if (condition) throw ExceptionClass(msg);
	}
};

}
}

#endif

