#!/usr/bin/python

import os

projectDir = os.environ['PROJECT_DIR']
vwnetHeaders = projectDir + "/../include/vwnet"

platformHeaderIn = vwnetHeaders + "/platform.hpp.in"
platformHeaderOut = vwnetHeaders + "/platform.hpp"

f = open(platformHeaderIn,'r')
filedata = f.read()
f.close()

newdata = filedata.replace("@PLATFORM_WINDOWS@","0")
newdata = newdata.replace("@PLATFORM_MAC@","1")
newdata = newdata.replace("@PLATFORM_LINUX@","0")
newdata = newdata.replace("@DIRECTX_AVAILABLE@","0")

f = open(platformHeaderOut,'w')
f.write(newdata)
f.close()
